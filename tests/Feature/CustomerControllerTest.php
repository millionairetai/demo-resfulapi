<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\CustomerRepository;
use App\Customer;

class CustomerControllerTest extends TestCase
{
    /**
     * Create customer.
     *
     * @return void
     */
    public function testCreateCustomer()
    {
        $response = $this->postJson('/api/customers', [
            'last_name' => 'Sally',
            'first_name' => 'Test',
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'last_name' => 'Sally',
                'first_name' => 'Test',
            ]);
    }

    /**
     * Get customer.
     *
     * @return void
     */
    public function testGetACustomer()
    {
        $response = $this->get('/api/customers/1');

        $response
            ->assertStatus(200)
            ->assertJson([
                'last_name' => 'Tran',
                'first_name' => 'Tai',
            ]);
    }

    /**
     * Get customer list
     *
     * @return void
     */
    public function testGetCustomerList()
    {
        $response = $this->get('/api/customers');

        $response
            ->assertStatus(200);
    }

    /**
     * Delete a customer
     *
     * @return void
     */
    public function testDeleteACustomer()
    {
        $lastCus = Customer::orderBy('created_at', 'desc')->first();
        $response = $this->delete("/api/customers/{$lastCus->id}");

        $response->assertStatus(200);
    }
}
