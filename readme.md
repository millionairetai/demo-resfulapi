# Laravel-restful-api
#### Application to add, delete, update, read a customer, 
## Steps to run

#### Install composer
``composer install``

#### Create example file .env from file .env 
``mv .env.example .env``

#### Configure enviroment var from file .env
``DB_DATABASE=``
``DB_USERNAME=``
``DB_PASSWORD= ``

#### Create key for app
``php artisan key:generate``

#### Run migration to create Database 
``php artisan migrate``

#### Run project with address http://localhost:8000 with command.
``php artisan serve``

#### Use postman to call api.
`` - GET - http://localhost:8000/api/customers/ - to get all customer.``
####
`` - GET - http://localhost:8000/api/customers/{customer-ID} - to get customer by id.``
####
`` - POST - http://localhost:8000/api/customers/ - With data is JSON: {"first_name":"","last_name":""} - to create new customer.``
####
`` - PUT - http://localhost:8000/api/customers/{customer-ID} - With data : {"first_name":"","last_name":""} - to update customer by id.``
####
`` - DELETE - http://localhost:8000/api/customers/{customer-ID} - To delete a customer.``
# 
