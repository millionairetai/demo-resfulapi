<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['first_name' => 'Tai', 'last_name' => 'Tran'],
            ['first_name' => 'Kien', 'last_name' => 'Tiet'],
            ['first_name' => 'Hung', 'last_name' => 'Trinh'],
            ['first_name' => 'Anh', 'last_name' => 'Tran Tuan 11111'],
            ['first_name' => 'Nghia22222', 'last_name' => 'Ta'],
            ['first_name' => 'Andreas', 'last_name' => 'AB'],
          ];

        DB::table('customers')->insert($data);

    }
}
